#!/usr/bin/env python3
import logging
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
import os.path
import pdb
import sys

sys.path.append('modules')

import home
import recovery
import sshKeys
import user

from tornado.options import define, options

define("port", default=3000, help="run on the given port", type=int)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('index.html')

class SocketHandler(tornado.websocket.WebSocketHandler):
    waiters = set()
    cache = []
    cache_size = 200
    recovery = recovery.Recovery()
    home = home.Home()
    sshKeys = sshKeys.SSHKeys()
    user = user.User()

    def open(self):
        SocketHandler.waiters.add(self)

    def on_close(self):
        SocketHandler.waiters.remove(self)

    @classmethod
    def send_updates(cls, message):
        logging.info("sending message to %d waiters", len(cls.waiters))

        for waiter in cls.waiters:
            try:
                waiter.write_message(tornado.escape.json_encode(message))
            except:
                logging.error("Error sending message", exc_info=True)

    def on_message(self, message):
        logging.info("got message %r", message)

        parsed = tornado.escape.json_decode(message)

        op = parsed['op'].split('.')
        module = op[0]
        op = op[1]

        if module == 'user':

            if op == 'signIn':
                parsed['data'] = self.user.signIn(parsed['email'], parsed['password'], parsed['rememberMe'])

        elif module == 'home':

            if op == 'getAllProcesses':
                parsed['data'] = self.home.getAllProcesses()
            elif op == 'getInfo':
                parsed['data'] = self.home.getInfo()
            elif op == 'stopProcess':
                parsed['data'] = self.home.stopProcess(parsed['process'])
            elif op == 'pauseProcess':
                parsed['data'] = self.home.pauseProcess(parsed['process'])
            elif op == 'playProcess':
                parsed['data'] = self.home.playProcess(parsed['process'])

        elif module == 'recovery':

            if op == 'getAllCheckpoints':
                parsed['data'] = self.recovery.getAllCheckpoints()
            elif op == 'getCurrentCheckpoint':
                parsed['data'] = self.recovery.getCurrentCheckpoint()
            elif op == 'restoreCheckpoint':
                parsed['data'] = self.recovery.restoreCheckpoint(parsed['checkpoint'])
            elif op == 'saveCheckpoint':
                parsed['data'] = self.recovery.saveCheckpoint(parsed['checkpoint'])

        elif module == 'sshKeys':

            if op == 'getAll':
                parsed['data'] = self.sshKeys.getAll()
            elif op == 'create':
                parsed['data'] = self.sshKeys.create(parsed['sshKey'])
            elif op == 'update':
                parsed['data'] = self.sshKeys.update(parsed['sshKey'])
            elif op == 'remove':
                parsed['data'] = self.sshKeys.remove(parsed['sshKey'])

        elif module == 'cluster':

            pass

        # send updates only to sender
        self.write_message(tornado.escape.json_encode(parsed))
        #SocketHandler.send_updates(parsed)

#===========================================================================

def main():
    tornado.options.parse_command_line()
    app = tornado.web.Application(
        [
            (r"/", MainHandler),
            (r"/([\w-]+.json)", tornado.web.StaticFileHandler, {'path': 'static/translations/'},),
            (r"/socket/?", SocketHandler),
        ],
        cookie_secret="43oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
        template_path=os.path.join(os.path.dirname(__file__), "views"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        xsrf_cookies=True,
        autoescape=None,
        debug=True,
    )
    app.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()