import tornado.escape
import sqlite3
import sys
import pdb
import datetime

class Recovery():
  checkpoints = [
    {
      'number': 1,
      'date': datetime.datetime.strptime('2013-05-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
      'name': 'owncloud-automatic',
      'comment': 'first check point',
    },
    {
      'number': 2,
      'date': datetime.datetime.strptime('2013-06-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
      'name': 'owncloud-automatic',
      'comment': 'second check point',
    },
    {
      'number': 3,
      'date': datetime.datetime.strptime('2013-07-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
      'name': 'owncloud-manual',
      'comment': 'second check point',
    },
    {
      'number': 4,
      'date': datetime.datetime.strptime('2013-08-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
      'name': 'owncloud-automatic',
      'comment': 'second check point',
    },
    {
      'number': 5,
      'date': datetime.datetime.strptime('2013-09-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
      'name': 'owncloud-automatic',
      'comment': 'second check point',
    },
    {
      'number': 6,
      'date': datetime.datetime.strptime('2013-10-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
      'name': 'owncloud-automatic',
      'comment': 'second check point',
    },
    {
      'number': 7,
      'date': datetime.datetime.strptime('2013-11-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
      'name': 'owncloud-manual',
      'comment': 'second check point',
    },
    {
      'number': 8,
      'date': datetime.datetime.strptime('2013-12-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
      'name': 'owncloud-automatic',
      'comment': 'second check point',
    },
    {
      'number': 9,
      'date': datetime.datetime.strptime('2014-01-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
      'name': 'owncloud-automatic',
      'comment': 'second check point',
    },
    {
      'number': 10,
      'date': datetime.datetime.strptime('2014-02-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
      'name': 'owncloud-automatic',
      'comment': 'second check point',
    },
  ]

  currentCheckpoint = checkpoints[-1]

  def getAllCheckpoints(self):
    return self.checkpoints

  def getCurrentCheckpoint(self):
    return self.currentCheckpoint

  def restoreCheckpoint(self, checkpoint):
    self.currentCheckpoint = checkpoint

  def saveCheckpoint(self, checkpoint):
    pass