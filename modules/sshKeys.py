import tornado.escape
import sqlite3
import sys
import pdb
import datetime

class SSHKeys():
  keys = [
    {
      'id': 1,
      'label': 'main',
      'key': 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDecTyBSPZq+qFJBL2dYJsfakOGrsJ2geJb4vdYW4DFWELlRpScEOtx8t9ct1EaGn+xzltok8UjyuBo5HEMt1eeI0wUP9kxz/nk0kpDb4I5GamBntZ6JULKRFc4GbJLAnudpoZRfVKBFDT7+u1yI1UB29o++k4oh6rWmRij/V9c7kdmE7g0RkjVeGvBMzVye+ggO5wBni6GvLVcvGLg4Xz4DOcQaFtjE425/s2xZRGcP0U0z/6GPDl9jeP/wAUveiOQQW0ByyeckDZmAyROgeZSWiiC/mUkImS2CwecBNJc4whcx0W46Dmr7otww/WmNghwtIuy1c7o9tDaAWazaX6f maxim@maxim',
      'added': datetime.datetime.strptime('2013-05-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
    },
    {
      'id': 2,
      'label': 'home',
      'key': 'ssh-rsa ABAAB3NzaC1yc2EAAAADAQABAAABAQDecTyBSPZq+qFJBL2dYJsfakOGrsJ2geJb4vdYW4DFWELlRpScEOtx8t9ct1EaGn+xzltok8UjyuBo5HEMt1eeI0wUP9kxz/nk0kpDb4I5GamBntZ6JULKRFc4GbJLAnudpoZRfVKBFDT7+u1yI1UB29o++k4oh6rWmRij/V9c7kdmE7g0RkjVeGvBMzVye+ggO5wBni6GvLVcvGLg4Xz4DOcQaFtjE425/s2xZRGcP0U0z/6GPDl9jeP/wAUveiOQQW0ByyeckDZmAyROgeZSWiiC/mUkImS2CwecBNJc4whcx0W46Dmr7otww/WmNghwtIuy1c7o9tDaAWazaX6f maxim123@maxim',
      'added': datetime.datetime.strptime('2014-02-01 12:00:00', '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S'),
    },
  ]

  def _getKeyInx(self, key):
    i = 0

    while i < len(self.keys):
      if self.keys[i]['id'] == key['id']:
        break

      i += 1

    return i

  def getAll(self):
    return self.keys;

  def create(self, key):
    key = {
      'id': self.keys[-1]['id'] + 1,
      'label': key['label'],
      'key': key['key'],
      'added': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
    }
    self.keys.append(key)
    return self.keys[-1]

  def update(self, key):
    keyInx = self._getKeyInx(key)

    key['added'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    self.keys[keyInx] = key

    return self.keys[keyInx]

  def remove(self, key):
    keyInx = self._getKeyInx(key)
    del self.keys[keyInx]
    return True