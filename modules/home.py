import tornado.escape
import sqlite3
import sys
import pdb
import datetime

class Home():
  processes = [
    {
      'name': 'Nginx',
      # 0 - не запущенный
      # 1 - работающий
      # 2 - приостановленный
      'status': 0,
    },
    {
      'name': 'MySQL',
      'status': 1,
    },
    {
      'name': 'PHP',
      'status': 2,
    },
  ]

  info = {
    'cpu': {
      'usage_value': '100%',
      'usage_percent': 100,
    },
    'hdd': {
      'usage_value': '8.5 GB',
      'usage_percent': 90,
    },
    'memory': {
      'usage_value': '750.0 Mb',
      'usage_percent': 40,
    },
  }

  def getAllProcesses(self):
    return self.processes

  def getInfo(self):
    return self.info

  def stopProcess(self, process):
    pass

  def pauseProcess(self, process):
    pass

  def playProcess(self, process):
    pass