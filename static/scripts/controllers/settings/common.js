'use strict';

angular.module('owncloudAdminPanel').controller('CommonCtrl', ['$scope', 'l10n', 'LOCALES', function ($scope, l10n, LOCALES) {
	var model = $scope.settings.common;

	if (!model.languages) {
		model.languages = {};
		model.languages.all = LOCALES;
		model.languages.current = l10n.getLocale();
	}

	$scope.$watch('settings.common.languages.current', function(val) {
		l10n.setLocale(val);
	});
}]);