'use strict';

angular.module('owncloudAdminPanel').controller('SSHKeysCtrl', ['$scope', 'sshKeysService', function ($scope, sshKeysService) {
	var model = $scope.settings.ssh,
			getAllKeys = function() {
				sshKeysService.getAllKeys().then(function(keys) {
					model.keys = keys;
				});
			}
		;

	model.currentKey = null;

	// SSH Keys -------------

	if (!model.keys) {
		getAllKeys();
	}

	$scope.create = function() {
		model.addition = !model.addition;

		if (model.addition) {
			model.currentKey = {
				key: '',
				label: '',
			};
		}
	};

	$scope.save = function() {
		var action;

		if (!model.currentKey.label) {
			model.currentKey.label = model.currentKey.key.substr(0, 100);
		}

		if (!model.currentKey.added) {
			action = 'create';
		} else {
			action = 'update';
		}

		sshKeysService[action](model.currentKey).then(function(key) {
			getAllKeys();
		});

		$scope.cancelEditing();
	};

	$scope.edit = function(SSHKey) {
		model.currentKey = angular.extend({}, SSHKey);
		model.addition = true;
	};

	$scope.cancelEditing = function() {
		model.currentKey = null;
		model.addition = false;
	};

	$scope.remove = function(SSHKey) {
		sshKeysService.remove(SSHKey).then(function() {
			getAllKeys();
		});

		$scope.cancelEditing();
	};
}]);