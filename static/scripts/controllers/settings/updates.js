'use strict';

angular.module('owncloudAdminPanel').controller('UpdatesCtrl', ['$scope', function ($scope) {
	var model = $scope.settings.updates;

	model.mainRepository = model.mainRepository || 'http://dl.google.com/linux/chrome/deb/';

	// periods -------------

	if (!model.periods) {
		model.periods = {};

		model.periods.all = [
			{name: 'Daily'},
			{name: 'Every two days'},
			{name: 'Weekly'},
			{name: 'Every two weeks'},
			{name: 'Never'},
		];

		model.periods.current = model.periods.all[0];
	}
}]);