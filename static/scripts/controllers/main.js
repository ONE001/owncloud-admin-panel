'use strict';

angular.module('owncloudAdminPanel').controller('MainCtrl', ['$scope', function ($scope) {
  $scope.loading = false;
  // инициализация моделей контроллеров
  // в родительском $scope
  $scope.home = {};
  $scope.recovery = {};
  $scope.cluster = {};
  $scope.settings = {
    common: {},
    password: {},
    ssh: {},
    updates: {},
  };

  $scope.showLoading = function() {
    $scope.loading = true;
  };

  $scope.hideLoading = function() {
    $scope.loading = false;
  };
}]);