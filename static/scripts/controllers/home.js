'use strict';

angular.module('owncloudAdminPanel').controller('HomeCtrl', ['$scope', 'homeService', function ($scope, homeService) {
  var model = $scope.home;

  if (!model.processes) {
    homeService.getAllProcesses().then(function(processes) {
      model.processes = processes;
    });
  }

  if (!model.info) {
    homeService.getInfo().then(function(info) {
      model.info = info;
    });
  }

  $scope.stopProcess = function(p) {
    homeService.stopProcess(p).then(function() {
      p.status = 0;
    });
  };

  $scope.pauseProcess = function(p) {
    homeService.pauseProcess(p).then(function() {
      p.status = 2;
    });
  };

  $scope.playProcess = function(p) {
    homeService.playProcess(p).then(function() {
      p.status = 1;
    });
  };
}]);