'use strict';

angular.module('owncloudAdminPanel').controller('LoginCtrl', ['$scope', '$state', 'userService', function ($scope, $state, userService) {
  var model = $scope.login || {
    'email': null,
    'password': null,
    'rememberMe': false,
  };

  $scope.signIn = function() {
    userService.signIn(model.email, model.password, model.rememberMe).then(function() {
      $state.go('home');
    }, function() {
      console.log('Email or password is incorrect');
    });
  };

  $scope.login = model;
}]);