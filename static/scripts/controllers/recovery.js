'use strict';

angular.module('owncloudAdminPanel').controller('RecoveryCtrl', ['$scope', 'recoveryService', function ($scope, recoveryService) {
	var model = $scope.recovery;

	if (!model.checkpoints) {
		// pre init
		model.checkpoints = {
			all: [],
			current: null,
		};
		model.pagination = {};

    recoveryService.getAllCheckpoints().then(function(checkpoints) {
      model.checkpoints.all = checkpoints;

      recoveryService.getCurrentCheckpoint().then(function(currentCheckpoint) {
        model.checkpoints.current = $.grep(model.checkpoints.all, function(checkpoint) {
          return checkpoint.number === currentCheckpoint.number;
        })[0];
      });

      model.pagination = {
        'totalItems': model.checkpoints.all.length,
        'currentPage': 1,
        'maxSize': 5, // количество видимых номеров страниц в строке пейджинга
        'itemsPerPage': 5,
      };
    });
	}

	$scope.restoreCheckpoint = function(checkpoint) {
    recoveryService.restoreCheckpoint(checkpoint).then(function() {
		  model.checkpoints.current = checkpoint;
    });
	};

	$scope.editCheckpoint = function(checkpoint) {
		model.checkpointEditing = checkpoint;
		model.form = model.form || {};
		model.form.checkpointName = checkpoint.name;
		model.form.checkpointComment = checkpoint.comment;
	};

	$scope.saveCheckpoint = function(checkpoint, name, comment) {
		checkpoint.name = name;
		checkpoint.comment = comment;
		$scope.closeEditing();
    recoveryService.saveCheckpoint(checkpoint);
	};

	$scope.closeEditing = function() {
		model.checkpointEditing = null;
	};

	var storedPageNumber;


  $scope.update = function(name) {
    if (name) {
      if (!storedPageNumber) {
        storedPageNumber = model.pagination.currentPage;
      }

      model.pagination.currentPage = 1;
    } else {
      model.pagination.currentPage = storedPageNumber || 1;
      storedPageNumber = null;
    }

    model.pagination.totalItems = model.filtered.length
  };
}]);