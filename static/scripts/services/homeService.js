"use strict";

angular.module('owncloudAdminPanel').service("homeService", ['WS', function(WS) {
  this.getAllProcesses = function() {
    return WS.send({'op': 'home.getAllProcesses'});
  };

  this.getInfo = function() {
    return WS.send({'op': 'home.getInfo'});
  };

  this.stopProcess = function(process) {
    return WS.send({'op': 'home.stopProcess', 'process': process});
  };

  this.pauseProcess = function(process) {
    return WS.send({'op': 'home.pauseProcess', 'process': process});
  };

  this.playProcess = function(process) {
    return WS.send({'op': 'home.playProcess', 'process': process});
  };
}]);