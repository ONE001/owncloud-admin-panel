"use strict";

angular.module('owncloudAdminPanel').service("sshKeysService", ['WS', function(WS) {
  this.getAllKeys = function() {
    return WS.send({'op': 'sshKeys.getAll',});
  };

  this.create = function(sshKey) {
    return WS.send({'op': 'sshKeys.create', 'sshKey': sshKey});
  };

  this.update = function(sshKey) {
    return WS.send({'op': 'sshKeys.update', 'sshKey': sshKey});
  };

  this.remove = function(sshKey) {
    return WS.send({'op': 'sshKeys.remove', 'sshKey': sshKey});
  };
}]);