"use strict";

angular.module('owncloudAdminPanel').provider("WS", function() {
    var url;

    this.setURL = function() {
        url = arguments[0];
    };

    this.$get = function(CONNECTION_LIMIT, RECONNECT_INTERVAL, $q, $timeout) {
        if (!url) {
            throw new Error("url must be specified");
        }

        var
            // сокет instance
            _socket,

            reconnect_intervals = [],
            // номер попытки реконнекта
            n = 0,
            // количество попыток реконнекта
            clear_intervals = function() {
                for(var i = reconnect_intervals.length - 1; i >= 0; i -= 1)
                   clearInterval(reconnect_intervals[i]);
                reconnect_intervals = [];
            },

            // Индикатор состояния соединения.
            ready = $q.defer(),

            // Объект, позволяющий организовать список функций обратного вызова в виде
            // в виде хеш таблицы. Это необходимо для того, чтобы четко сопоставлять
            // каждому запросу соответствующий ему ответ, который, в свою очередь,
            // будет использоваться в качестве входного параметра функции обратного
            // вызова.
            _handlers = function() {
                var _methods = {};

                return {
                    // добавляет в объект функцию обратного вызова под уникальным ключом
                    add: function(key, callback, counter) {
                        if (typeof key === "string" && typeof callback === "function") {
                            if (!_methods[key]) {
                                _methods[key] = callback;
                                return key;
                            } else {
                                counter = counter || 1;
                                return this.add(key + counter, callback, counter + 1);
                            }
                        } else {
                            return false;
                        }
                    },
                    get: function(key) {
                        return _methods[key];
                    },
                    // извлекаем обработчик, в прямом смысле, так что после извлечения
                    // его не останется в общем объекте
                    retrieve: function(key) {
                        var handler = _methods[key];
                        delete _methods[key];
                        return handler;
                    },
                };
            }(),

            connect = function() {
                var
                    __socket = (function() {
                        if ('WebSocket' in window)
                            return new WebSocket(url);
                        else
                            return new MozWebSocket(url);
                    }())
                ;

                /* Переопределение стандартных обработчиков событий объекта WebSocket:
                   - соединение с сервером установлено;
                   - соединение с сервером разорвано;
                   - клиент получил от сервера, по установленному ранее каналу связи,
                   сообщение. */
                __socket.onopen = function() {
                    var request;

                    console.info("established connection");
                    ready.resolve();
                    n = 0;
                    clear_intervals();
                };

                __socket.onclose = function() {
                    console.info("closed connection");
                    ready = $q.defer();

                    reconnect_intervals.push(
                        $timeout(
                            function() {
                                n += 1;

                                if (n >= CONNECTION_LIMIT)
                                    throw new Error("Connection failed!");
                                else
                                    _socket = connect();

                                return 1;
                            },
                            RECONNECT_INTERVAL
                        )
                    );
                };

                __socket.onmessage = function(event) {
                    // общение с сервером идет посредством JSON,
                    // поэтой причине парсинг ответа перенес сюда.
                    // иначе же зачем делать одну и туже работу в каждом методе
                    // шлющем запрос на сервер
                    var json = JSON.parse(event.data);

                    // и проверка на серверные ошибки в ответе тоже здесь
                    // дабы избежать дублирования
                    if (json.error) {
                        throw new Error(json.reason);
                    }

                    var current = _handlers.retrieve(json.key || json.op);

                    if (typeof current !== "function") {
                        return;
                    }

                    return current(json.data);
                };

                return __socket;
            },
            res = {
                send: function(data, callback) {
                    var deferred = $q.defer();

                    $q.when(ready.promise).then(function() { // ready.promise.then выполнится когда соединение будет установленно
                        var resolve = function() {
                            if (typeof callback === "function") {
                                callback.apply(this, arguments);
                            }

                            deferred.resolve.apply(this, arguments);
                        };

                        if (typeof data !== "object") {
                            throw new Error("The parameter 'data' must be an Object!");
                        }

                        // Сохраняем функцию обратного вызова в очереди.
                        // и получаем ключ сохранения обработчика, который
                        // будет отправлен на сервер и возвращен, и по нему
                        // будет извлечен этот самый обработчик при обработке ответа
                        data.key = _handlers.add(data.op, resolve);

                        data = JSON.stringify(data);

                        _socket.send(data);

                    });

                    return deferred.promise;
                },
            }
        ;

        _socket = connect.call(this);

        return res;
    };
});
