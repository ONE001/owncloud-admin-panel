"use strict";

angular.module('owncloudAdminPanel').service("recoveryService", ['WS', function(WS) {
  this.getAllCheckpoints = function() {
    return WS.send({'op': 'recovery.getAllCheckpoints',});
  };

  this.getCurrentCheckpoint = function() {
    return WS.send({'op': 'recovery.getCurrentCheckpoint',});
  };

  this.restoreCheckpoint = function(checkpoint) {
    return WS.send({'op': 'recovery.restoreCheckpoint', 'checkpoint': checkpoint});
  };

  this.saveCheckpoint = function(checkpoint) {
    return WS.send({'op': 'recovery.saveCheckpoint', 'checkpoint': checkpoint});
  };
}]);