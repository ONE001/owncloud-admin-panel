"use strict";

angular.module('owncloudAdminPanel').service("userService", ['WS', '$q', function(WS, $q) {
  this.signIn = function(email, password, rememberMe) {
    var defer = $q.defer();

    WS.send({'op': 'user.signIn', 'email': email, 'password': password, 'rememberMe': rememberMe}).then(function(status) {
      if (status) {
        defer.resolve();
      } else {
        defer.reject();
      }
    });

    return defer.promise;
  };

  this.updatePassword = function() {

  };

  this.checkPassword = function() {

  };

  this.isAuthenticated = function() {

  };
}]);