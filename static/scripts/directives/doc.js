'use strict';

angular.module('owncloudAdminPanel').directive('doc', ['DOCUMENTATION_LINK', function(DOCUMENTATION_LINK) {
  return {
    restrict: 'A',
    scope: {
      link: '@doc',
    },
    link: function($scope, element) {
      var $a = $('<a>'),
          link = $scope.link;

      if (!link) {
        link = element.text().replace(/\s/g, '');
      }

      link = DOCUMENTATION_LINK.replace('%hash%', link || '');

      $a.prop('href', link);
      $a.prop('target', '_blank');
      $a.text('[?]');

      element.append($a);
    }
  };
}]);