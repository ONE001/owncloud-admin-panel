'use strict';

angular.module('owncloudAdminPanel').directive('navbar', ['TEMPLATE_PATH', function(TEMPLATE_PATH) {
  return {
    restrict: 'A',
    templateUrl: TEMPLATE_PATH + 'navbar.html',
  };
}]);