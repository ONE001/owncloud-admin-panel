'use strict';

angular.module('owncloudAdminPanel')
  .config(['$stateProvider', '$urlRouterProvider', '$provide', 'l10nProvider', 'WSProvider', function ($stateProvider, $urlRouterProvider, $provide, l10n, WS) {
    var templatePath = '/static/views/';
    // путь к шаблонам
    $provide.constant('TEMPLATE_PATH', templatePath);
    // количество попыток реконнекта
    $provide.constant('CONNECTION_LIMIT', 10);
    // время между реконнектами
    $provide.constant('RECONNECT_INTERVAL', 1000);

    WS.setURL('ws://' + location.host + '/socket');

    $provide.constant('DOCUMENTATION_LINK', 'http://documentation.com/#%hash%');

    // возможные для переключения локали. Необходимо, что бы локали с такими же именами лежали в translations/
    $provide.constant('LOCALES', {
      'en-us': 'English',
      'ru-ru': 'Russian',
    });

    l10n.pathToFile('/');

    $urlRouterProvider.when('/settings', '/settings/common');
    $urlRouterProvider.otherwise('/login');

    // routes
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: templatePath + 'login.html',
        controller: 'LoginCtrl',
      })
      .state('home', {
        url: '/home',
        templateUrl: templatePath + 'home.html',
        controller: 'HomeCtrl',
      })
      .state('recovery', {
        url: '/recovery',
        templateUrl: templatePath + 'recovery.html',
        controller: 'RecoveryCtrl',
      })
      .state('settings', {
        url: '/settings',
        templateUrl: templatePath + 'settings/settings.html',
      })
        .state('settings.common', {
          url: '/common',
          templateUrl: templatePath + 'settings/settings.common.html',
          controller: 'CommonCtrl',
        })
        .state('settings.updates', {
          url: '/updates',
          templateUrl: templatePath + 'settings/settings.updates.html',
          controller: 'UpdatesCtrl',
        })
        .state('settings.ssh_keys', {
          url: '/ssh_keys',
          templateUrl: templatePath + 'settings/settings.ssh_keys.html',
          controller: 'SSHKeysCtrl',
        })
        .state('settings.password', {
          url: '/password',
          templateUrl: templatePath + 'settings/settings.password.html',
          controller: 'PasswordCtrl',
        })
      .state('cluster', {
        url: '/cluster',
        templateUrl: templatePath + 'cluster.html',
        controller: 'ClusterCtrl',
      });
  }])

  .run(['$locale', 'l10n', function($locale, l10n) {
    // default locale
    l10n.setLocale($locale.id);
  }])
;